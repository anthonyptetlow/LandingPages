var gulp = require('gulp'),
	server = require('gulp-server-livereload');

gulp.task('server', function() {
  gulp.src('./')
    .pipe(server({
      livereload: true,
      port: 8000
    }));
});
