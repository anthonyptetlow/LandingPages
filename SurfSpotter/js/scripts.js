(function () {
	var submittButton = document.getElementById('register-form-submit');
	console.log(submittButton);
	submittButton.onclick = function (event) {
		event.preventDefault();
		var email = document.getElementById('email');
		minAjax({
		    url:'./api/register',
		    type:'POST',
		    data:{
		      email: email.value,
		      projectKey: 'surfspotter'
		    },
		    success: function(){
		    	var form = document.getElementById('register-form');
		    	form.className = 'hide';
		    	var feedback = document.getElementById('feedback-success');
		    	feedback.className = '';
		    }
		});
	}
})();
